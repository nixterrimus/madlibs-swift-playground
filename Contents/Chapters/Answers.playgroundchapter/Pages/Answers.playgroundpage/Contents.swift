//: Use ``show()`` to output text and ``ask()`` to input text.
//#-code-completion(identifier, hide, AnswersLiveViewCommand)

show("What is your name?")

let name = ask("Name")

show("Hi " + name)
